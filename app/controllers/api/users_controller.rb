class Api::UsersController < ApplicationController

    before_action :getUser , only: [:ShowUser, :UpdateUser, :DeleteUser]

    def AddUser
        user = User.new(userparams)

        if user.save()
            render json: user, status: :ok
         
        else
           render json: {message: "unable to add user"}, status: :unprocessable_entity
         
        end
    end

    def GetUsers
        user = User.all

        if user
            render json: user, status: :ok
        else
            render json: {message: "users not found"}, status: :unprocessable_entity
        end 
    end

    def ShowUser
        if  @user 
            
            render json: @user, status: :ok
            
        else

            render json: {message: "user not found"}, status: :unprocessable_entity

        end
    end

    def UpdateUser
        if  @user 
            if @user.update(userparams)
 
              render json: @user, status: :ok
 
            else
 
              render json: {message: "update failed"}, status: :unprocessable_entity
 
            end
        else
            
            render json: {message: "user not found"}, status: :unprocessable_entity

        end
    end

    def DeleteUser
        if  @user 

            if @user.destroy()

              render json: { message: "user deleted"}, status: :ok

            else

              render json: {message: "update failed"}, status: :unprocessable_entity

            end

        else

            render json: {message: "user not found"}, status: :unprocessable_entity

        end

    end

    def SearchUsers
         
        @parameter = params[:input]
        @output = User.or({firstName: /#{@parameter}/i}, {lastName: /#{@parameter}/i}, {email: /#{@parameter}/i})
        render json: @output, status: :ok 

    end

    private
  
    def userparams

        params.permit(:firstName, :lastName, :email)

    end

    def getUser

        @user = User.find(params[:id])
        
    end

end
