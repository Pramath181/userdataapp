Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  namespace :api do
    post 'user', action: :AddUser, controller: :users
    get 'users', action: :GetUsers, controller: :users
    get 'user', action: :ShowUser, controller: :users
    put 'user', action: :UpdateUser, controller: :users
    delete 'user', action: :DeleteUser, controller: :users
    get 'typeahead', action: :SearchUsers, controller: :users
  end
end
